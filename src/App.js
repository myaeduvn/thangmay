import React , {useState,useEffect} from 'react'
import {Row,Col,Button} from 'antd'
import {UpOutlined,DownOutlined,LeftOutlined,RightOutlined} from '@ant-design/icons'
import './App.css';
import 'antd/dist/antd.css';
import ElevatorComponent from './components/elevator';
const ElevatorMainName = 'SG123456789'
const Settings = {
  Floor:5,
  FloorName:['4','3','2','1','G'],
  ElevatorName:['3','4','1','2','G']
}
function App(){
  const [Action, setAction] = useState({
    OutsideAction:[0,0,0,0,0,0,0,0,0,0],
    InsideActionA:[0,0,0,0,0,0,0,0,0,0],
    InsideActionB:[0,0,0,0,0,0,0,0,0,0]
  })

  const [queeElevatorA, setqueeElevatorA] = useState([1,1])
  const [queeElevatorB, setqueeElevatorB] = useState([1,1])
  const [doorA, setDoorA] = useState(true)
  const [doorB, setDoorB] = useState(true)

  const setOutsideAction = (floor,handle) => {
    const OutsideAction = Action.OutsideAction
    switch (handle) {
      case 1:
        OutsideAction[floor] = Action.OutsideAction[floor] === 0 ? 1 : Action.OutsideAction[floor] === 2 ? 3 : Action.OutsideAction[floor]
        break;
      case 2:
        OutsideAction[floor] = Action.OutsideAction[floor] === 0 ? 2 : Action.OutsideAction[floor] === 1 ? 3 : Action.OutsideAction[floor]
        break;
      default:
        break;
    }
    if(queeElevatorA[0] === queeElevatorB[0] && queeElevatorA[0] === queeElevatorA[1]){
      if(floor === queeElevatorA[0]){
        setDoorA(true)
        return;
      }
      setqueeElevatorA(x=>[x[1],floor])
    } else {
      if(queeElevatorA[0] === queeElevatorA[1]){
        setqueeElevatorA(x=>[queeElevatorA[1],floor])
        setAction(x =>{ return {...x,OutsideAction}})
        return;
      } else if (queeElevatorB[0] === queeElevatorB[1]){
        setqueeElevatorB(x=>[queeElevatorA[1],floor])
        setAction(x =>{ return {...x,OutsideAction}})
        return;
      }
    }
    setAction(x =>{ return {...x,OutsideAction}})
  }

  const sentInsideElevator = async (floor,ElevatorNumber) => {
    if(ElevatorNumber === 'A'){
      const InsideActionA = Action.InsideActionA
      InsideActionA[floor] = 1
      if(queeElevatorA[0] === queeElevatorA[1]){
        if(floor === queeElevatorA[1]){
          setDoorA(true)
          InsideActionA[floor] = 0
          await sleep(2000)
        } else {
          
          setqueeElevatorA(x=>[x[1],floor])
        }
      } 
      setAction(x => {return{...x,InsideActionA}})
    } else {
      const InsideActionB = Action.InsideActionB
      InsideActionB[floor] =1
      if(queeElevatorB[0] === queeElevatorB[1]){
        if(floor === queeElevatorB[1]){
          setDoorB(true)
          InsideActionB[floor] =0
          await sleep(2000)
        } else {
          setqueeElevatorB(x=>[x[1],floor])
        }
      }
      setAction(x => {return{...x,InsideActionB}})
    }

  }
  const setCompletFloor =async (element) => {
    const OutsideAction = Action.OutsideAction
    if(element === 'A'){
      setDoorA(true)
      await sleep(2000)
      const temp = queeElevatorA
      const increment = temp[1] - temp[0]
      let nextA = temp[1]
      if(increment===0){
        for (let i = 0; i < Settings.Floor; i++) {
          if(Action.InsideActionA[i] > 0 || OutsideAction[i]>0){
            nextA = i
            break;
          }
        }
      } else if(increment>0){
        for (let i = 0; i <= Settings.Floor; i++) {
          if((Action.InsideActionA[i] > 0 && i > nextA) || OutsideAction[i] === 1 || OutsideAction[i] === 3){
            nextA = i
            break;
          }
        }
        if(nextA === temp[1]){
          for (let i = Settings.Floor; i > 0; i--) {
            if((Action.InsideActionA[i] > 0 && i < nextA) || OutsideAction[i] === 2 || OutsideAction[i] === 3){
              nextA = i
              break;
            }
          }
        }
      } else {
        for (let i = Settings.Floor; i > 0; i--) {
          if((Action.InsideActionA[i] > 0 && i < nextA) || OutsideAction[i] === 2 || OutsideAction[i] === 3 ){
            nextA = i
            break;
          }
        }
        if(nextA === temp[1]){
          for (let i = 0; i <= Settings.Floor; i++) {
            if((Action.InsideActionA[i] > 0 && i > nextA) || OutsideAction[i] === 1 || OutsideAction[i] === 3 ){
              nextA = i
              break;
            }
          }
        }
      }

      const InsideActionA = Action.InsideActionA
      InsideActionA[temp[1]] = 0
      
      setqueeElevatorA([temp[1],nextA])
      setAction(x => {
        const OutsideAction = x.OutsideAction
        if(temp[1] === nextA){
          OutsideAction[temp[1]] = 0
        } else if(temp[1]>nextA){
          OutsideAction[temp[1]] = OutsideAction[temp[1]] - 2
        } else {
          OutsideAction[temp[1]] = OutsideAction[temp[1]] - 1
        }
        return {...x,OutsideAction,InsideActionA}
      })
    }else {
      setDoorB(true)
      await sleep(2000)
      const temp = queeElevatorB
      const increment = temp[1] - temp[0]
      let nextB = temp[1]
      if(increment===0){
        for (let i = 0; i < Settings.Floor; i++) {
          if(Action.InsideActionB[i] > 0){
            nextB = i
          }
        }
      } else if(increment>0){
        for (let i = 0; i <= Settings.Floor; i++) {
          if((Action.InsideActionB[i] > 0 && i > nextB) || OutsideAction[i] === 1 || OutsideAction[i] === 3){
            nextB = i
            break;
          }
        }
        if(nextB === temp[1]){
          for (let i = Settings.Floor; i > 0; i--) {
            if((Action.InsideActionB[i] > 0 && i < nextB) || OutsideAction[i] === 2 || OutsideAction[i] === 3){
              nextB = i
              break;
            }
          }
        }
      } else {
        for (let i = Settings.Floor; i > 0; i--) {
          if((Action.InsideActionB[i] > 0 && i < nextB) || OutsideAction[i] === 2 || OutsideAction[i] === 3){
            nextB = i
            break;
          }
        }
        if(nextB === temp[1]){
          for (let i = 0; i <= Settings.Floor; i++) {
            if((Action.InsideActionB[i] > 0 && i > nextB) || OutsideAction[i] === 1 || OutsideAction[i] === 3){
              nextB = i
              break;
            }
          }
        }
      }
      const InsideActionB = Action.InsideActionB
      InsideActionB[temp[1]] = 0
      setqueeElevatorB(x => [temp[1],nextB])
      setAction(x => {
        if(temp[1] === nextB){
          OutsideAction[temp[1]] = 0
        } else if(temp[1]>nextB){
          OutsideAction[temp[1]] = OutsideAction[temp[1]] - 2
        } else {
          OutsideAction[temp[1]] = OutsideAction[temp[1]] - 1
        }
        return {...x,OutsideAction,InsideActionB}
      })
    }
  }
  function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  return(
    <div style={{backgroundColor:"#808080",width:'100vw',height:'100vh',position:'relative'}}>
      {Settings.FloorName.map((x,i) => 
        <Row style={{height:`${100/Settings.Floor}vh`,borderBottom:'10px',borderColor:'white'}}>
          <Col className="fill100percent">
            <Row className="fill100percent" style={{alignItems: 'center',justifyContent:'center',backgroundColor:`${i===Settings.Floor-1?'#e6f6ff':i%2===0?'#e8fffb':'#f7ffee'}`}}>
              <Col xs={4} style={{alignItems: 'center',justifyContent:'center'}}>
                <span>Tang {x}</span> {" "}
                {i > 0 ? i < Settings.Floor-1 ? 
                  <>
                    <Button shape="circle" type={Action.OutsideAction[Settings.Floor-i]===1 | Action.OutsideAction[Settings.Floor-i]===3?"primary":"default"} icon={<UpOutlined />} onClick={()=>setOutsideAction(Settings.Floor-i,1)}/>{" "}
                    <Button shape="circle" type={Action.OutsideAction[Settings.Floor-i]===2 | Action.OutsideAction[Settings.Floor-i]===3?"primary":"default"} icon={<DownOutlined />} onClick={()=>setOutsideAction(Settings.Floor-i,2)}/> 
                  </>
                  :  <Button shape="circle" type={Action.OutsideAction[Settings.Floor-i]===1?"primary":"default"} icon={<UpOutlined />} onClick={()=>setOutsideAction(Settings.Floor-i,1)}/>
                :<Button shape="circle" type={Action.OutsideAction[Settings.Floor-i]===2?"primary":"default"} icon={<DownOutlined />} onClick={()=>setOutsideAction(Settings.Floor-i,2)}/>}
              </Col>
              <Col xs={16}>

              </Col>
            </Row>
          </Col>
        </Row>
      )}
      <ElevatorComponent name="Elevator A" Floor={Settings.Floor} door={doorA} setdoor={(x) => setDoorA(x) } nextFloor={queeElevatorA[1]} leftElevator='25vw' isComplete={(i)=>setCompletFloor('A')}/>
      <Row className="elevator" style={{top:`25%`,left:'38vw',paddingTop:'5vh',paddingBottom:'5vh',justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
        <Col xs={24} style={{textAlign:'center',padding:'5px'}}>
          <span>Thang máy A</span>
        </Col>
        { Settings.ElevatorName.map((x,i) => 
          <>
            {i === Settings.Floor-1 ? 
              <Col xs={24} style={{textAlign:'center',padding:'5px'}}>
                <Button shape="round" type={Action.InsideActionA[ElevatorMainName.indexOf(x)] === 1 ? 'primary' : 'default'} onClick={()=>sentInsideElevator(ElevatorMainName.indexOf(x),'A')} >{x}</Button>
              </Col> : 
              <Col xs={12} style={{textAlign:`${i%2===0?'end':'start'}`,padding:'5px'}}>
                <Button shape="round" type={Action.InsideActionA[ElevatorMainName.indexOf(x)] === 1 ? 'primary' : 'default'} onClick={()=>sentInsideElevator(ElevatorMainName.indexOf(x),'A')}>{x}</Button>
              </Col>}
          </>
        )}
        <Col xs={12} style={{textAlign:'end',padding:'5px'}}>
          <Button shape="round" onClick={() => setDoorA(true)}>
          <LeftOutlined />
          <RightOutlined />
          </Button>
        </Col>
        <Col xs={12} style={{textAlign:'start',padding:'5px'}}>
          <Button shape="round" onClick={() => setDoorA(false)}>
            <RightOutlined />
            <LeftOutlined />
          </Button>
        </Col>
      </Row>
      <ElevatorComponent name="Elevator B" Floor={Settings.Floor} door={doorB} setdoor={(x) => setDoorB(x) } nextFloor={queeElevatorB[1]} leftElevator='60vw' isComplete={(i)=>setCompletFloor('B')}/>
      <Row className="elevator" style={{top:`25%`,left:'78vw',paddingTop:'5vh',paddingBottom:'5vh',justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
        <Col xs={24} style={{textAlign:'center',padding:'5px'}}>
          <span>Thang máy B</span>
        </Col>
        { Settings.ElevatorName.map((x,i) => 
          <>
          {i === Settings.Floor-1 ? 
            <Col xs={24} style={{textAlign:'center',padding:'5px'}}>
              <Button shape="round" type={Action.InsideActionB[ElevatorMainName.indexOf(x)] === 1 ? 'primary' : 'default'} onClick={()=>sentInsideElevator(ElevatorMainName.indexOf(x),'B')} >{x}</Button>
            </Col> : 
            <Col xs={12} style={{textAlign:`${i%2===0?'end':'start'}`,padding:'5px'}}>
              <Button shape="round" type={Action.InsideActionB[ElevatorMainName.indexOf(x)] === 1 ? 'primary' : 'default'} onClick={()=>sentInsideElevator(ElevatorMainName.indexOf(x),'B')}>{x}</Button>
            </Col>}
        </>
        )}
        <Col xs={12} style={{textAlign:'end',padding:'5px'}}>
          <Button shape="round" onClick={() => setDoorB(true)}>
          <LeftOutlined />
          <RightOutlined />
          </Button>
        </Col>
        <Col xs={12} style={{textAlign:'start',padding:'5px'}}>
          <Button shape="round" onClick={() => setDoorB(false)}>
            <RightOutlined />
            <LeftOutlined />
          </Button>
        </Col>
      </Row>
    </div>
  )
}


export default App