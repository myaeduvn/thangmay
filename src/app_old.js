import React,{useState,useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import { Layout,Row, Col,Form, Input, Button ,InputNumber,Typography} from 'antd';
import { FormInstance } from 'antd/lib/form';
import {UpOutlined,DownOutlined} from '@ant-design/icons'
const _ = require('lodash')
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 12 }
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 }
};
const ten = ['B1','G','0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20']
function UpperElevator(x){
  let result=0;
  switch (x) {
    case 0: case 4:
      result=x+1;
      break;
    case 1: case 5:
      result=x-1;
      break;
    case 2:case 6:
      result= x+1;
      break;
    case 3: case 7:
      result=x-1;
      break;
    default:
      break;
  }
  return result
}
function DownElevator(x){
  let result=0;
  switch (x) {
    case 0: case 4:
      result=x+2;
      break;
    case 1: case 5:
      result=x+2;
      break;
    case 2: case 6:
      result=x-2;
      break;
    case 3: case 7:
      result=x-2;
      break;
  }
  return result
}
function ElevatorClick(x){
  if(x<4){
    return x + 4
  } else{
    return x - 4
  }
}
let handled = []
function App() {
  const [soTang, setsoTang] = useState(3)
  const [Tang, setTang] = useState([0,0,0])
  const [Timesleep, setTimesleep] = useState(2000)
  const [ThangMay, setThangMay] = useState(1)
  const [Current, setCurrent] = useState(0)
  const [FlagUpDown, setFlagUpDown] = useState(0)
  const [OpenClose, setOpenClose] = useState(false)
  const [Holdon, setHoldon] = useState(false)
  useEffect(() => {
    let change = soTang - Tang.length
    if(change>0){
      for (let index = 1; index <= change; index++) {
        setTang((T) => [...T,0])
      }
    } else if(change<0){
      setTang(T => T.filter((x,i) => i !== T.length-1))
    }
  }, [soTang])
  useEffect(() => {
    if(Tang.filter(x => x>0).length>0){
       Tang.map((x,indez) => {
        if(x>0){
          return indez
        }
      })
      setHoldon(true)
    } else {
      setHoldon(false)
    }
  }, [Tang])
  const setTangHandle = (index,value) => {
    const t = Tang 
    t[index] = value
    setTang(t)
    handled.push(index)
  }
  useEffect(() => {
    async function runinside(){
      if(Current > FlagUpDown){
        switch (Tang[Current]) {
          case 1: case 5: case 7:
            await setTang(T => T.map((xx,index)=> index === Current ? 0 : xx));
            setOpenClose(true)
            await new Promise(resolve => setTimeout(resolve, 10000));
            setOpenClose(false)
            break;
          case 4: case 6: 
            await setTang(T => T.map((xx,index)=> index === Current ? xx-4 : xx));
            setOpenClose(true)
            await new Promise(resolve => setTimeout(resolve, 10000));
            setOpenClose(false)
            break;
          default:
            break;
        }
      } else {
        switch (Tang[Current]) {
          case 4: case 5:
            await setTang(T => T.map((xx,index)=> index === Current ? xx-4 : xx));
            setOpenClose(true)
            await new Promise(resolve => setTimeout(resolve, 10000));
            setOpenClose(false)
            break;
          case 6: case 7: case 2:
            await setTang(T => T.map((xx,index)=> index === Current ? 0 : xx));
            setOpenClose(true)
            await new Promise(resolve => setTimeout(resolve, 10000));
            setOpenClose(false)
            break;
          default:
          break;
        }
      }
      if(Holdon){
        loopRunning()
      }
    }
    runinside()
  }, [Current])
  useEffect(() => {
    if(Holdon){
      loopRunning()
    }
  }, [Holdon])
  const loopRunning = () => {
    setTimeout(async () => {
      if(Holdon && Current<Tang.length){
        console.log(FlagUpDown + ' - ' + Current)
        if(FlagUpDown ){
          await setCurrent(Current+1)
        } else {
          await setCurrent(Current-1)
        }
      }
    }, Timesleep);
  }
  
  return (
    <Layout>
      <Layout.Header style={{backgroundColor:'white'}}>
        <Form>
          <Row>
            <Col>
              <Form.Item label="So luong tang">
                <InputNumber min={3} max={20} defaultValue={3} onChange={(x) => setsoTang(x)} />
              </Form.Item>
            </Col>
            <Col>
              <Form.Item label="So luong thang may">
                <InputNumber min={1} max={3} defaultValue={1} onChange={(x) => setThangMay(x)} />
              </Form.Item>
            </Col>
            <Col>
              <Form.Item label="Toc do ms">
                <InputNumber min={2000} max={50000} defaultValue={100} onChange={(x) => setTimesleep(x)} />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Layout.Header>
      <Layout.Content style={{backgroundColor:'white'}}>
        <Form {...layout}>
          <Row>
            <Col span={12}>
              <Typography>
                <Typography.Title>
                  Tầng {ten[Current===Tang.length?Current-1:Current]} -- {OpenClose ? 'Opening' : 'Closed'}
                </Typography.Title>
              </Typography>
              
              <Row>
                <Col span={12}>
                  <Row>
                    {_.isEmpty(Tang) || !_.isArray(Tang) ? null :
                    Tang.map((element,i) => {
                      return (
                        <Col span={12} style={{padding:10}} key={i}>
                          <Button type="default" shape="circle" type={element >= 4 ? "primary" : "default"} 
                            onClick={()=> setTang(T => T.map((x,index) => index == i ? ElevatorClick(T[i]) : x))}
                          >
                            {ten[i]}
                          </Button>
                        </Col>
                      )
                    })}
                  </Row>
                </Col>
                <Col span={12}>
                </Col>
              </Row>
            </Col>
            <Col span={12}>
              {_.isEmpty(Tang) || !_.isArray(Tang) ? null : Tang.map((element,i) => {
                return (
                  <Form.Item {...tailLayout}
                    style={{borderTopWidth:10,borderColor:'red',padding:10,marginRight:20}}
                    label={<label style={{color:'red'}}>Taang {ten[i]}</label> }
                  >
                    {i==Tang.length-1 ? null : <Button type={[1,3,5,7].includes(element) ? "primary" : "default"} 
                      shape="circle"
                      onClick={()=> setTang(T => T.map((x,index) => index == i ? UpperElevator(T[i]) : x))}
                        icon={<UpOutlined/>}
                        />}
                    
                    {" "}
                    {i==0 ? null : <Button type={[2,3,6,7].includes(element) ? "primary" : "default"} 
                      onClick={()=> setTang(T => T.map((x,index) => index == i ? DownElevator(T[i]) : x))}
                      shape="circle"
                      icon={<DownOutlined/>}
                      />}
                    
                  </Form.Item>
                )
              }).reverse()}
            </Col>
          </Row>
        </Form>
        
      </Layout.Content>
    </Layout>
  );
}

export default App;
