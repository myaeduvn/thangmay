import React,{useEffect,useState} from 'react';
import {Row,Col} from 'antd';

const ElevatorName = 'SG123456789'


function Elevator(props){
  const [doorAction, setdoorAction] = useState(89)
  const [currentFloor, setcurrent] = useState(1)

  const [elevatorAction, setelevatorAction] = useState(100/props.Floor)
  useEffect(() => {
    animateDoor(props.door)
  }, [props.door])
  function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  useEffect(() => {
    async function abc (){
      if(props.nextFloor>0){
        if(props.nextFloor!==currentFloor){
          if(props.door){
            await props.setdoor(!props.door)
            await sleep(2000)
          }
          await animationElevator(props.nextFloor)
        }
      }
    }
    abc()
  }, [props.nextFloor])
  useEffect(() => {
    props.isComplete(props.nextFloor)
  }, [currentFloor])
  function animateDoor(open){
    let current = open ? 0 : 89;
    let end = open ? 89 : 0;
    let increment = open ? 1 : -1;
    const step = function() {
      current += increment;
      setdoorAction(current)
      if (current !== end) {
        setTimeout(step, ((2000) / Math.pow(89, 3)) * Math.pow(current, 2));
      }
    }
    setTimeout(step, ((2000) / Math.pow(89, 3)) * Math.pow(open ? 0 : 89, 2));
  }
  function animationElevator(nextFloor){

    let current = (100/props.Floor)*currentFloor
    let end = (100/props.Floor)*nextFloor
    let range = Math.abs(current - end)
    let increment = current < end ? 1 : -1;
    const step = function() {
      current += increment;
      setelevatorAction(current)
      if (current !== end) {
        setTimeout(step, 0.1 * (100 + range));
      }else {
        props.setdoor(!props.door)
        sleep(2000)
        setcurrent(nextFloor)
        
      }
    }
    setTimeout(step, 0.1 * (100 + range));

  }
  return(
    <Row className="ElevatorA" style={{top:`${100-elevatorAction}%`,left:`${props.leftElevator}`,height:`${100/props.Floor}vh`}}>
      <span className="elevatorName">Tầng {ElevatorName[currentFloor]}</span>
      <Col xs={12} style={{left:`-${doorAction}px`,backgroundColor:'#9b640b'}}>
      </Col>
      <Col xs={12} style={{right:`-${doorAction}px`,backgroundColor:'#9b640b'}}>
      </Col>
    </Row>
  )
}

export default Elevator